# Copyright 2014-2018 Kylie McClain <somasis@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require bash-completion zsh-completion
require setup-py [ import="setuptools" blacklist='2' multibuild=false python_opts='[sqlite]' test='pytest' ]
ever is_scm || require pypi
require github [ user="beetbox" release="v${PV}" suffix="tar.gz" ]

export_exlib_phases src_{compile,install}

SUMMARY="The music geek's media organizer"

SLOT="0"
LICENCES="MIT"

UPSTREAM_CHANGELOG="${HOMEPAGE}/releases [[ lang = en ]]"
UPSTREAM_DOCUMENTATION="
    http://docs.beets.io/en/v${PV}/ [[ lang = en ]]
"

# TODO(somasis): it would be nice to include the depdenency on acousticbrainz' extractor.
# however, they specifically request that people use their static binaries currently.
# once they have a test suite to ensure the built extractor gives the same results, then
# it should be packaged...
#
# https://acousticbrainz.org/faq

DEPENDENCIES="
    build+run:
        dev-python/PyYAML[python_abis:*(-)?]
        dev-python/Unidecode[python_abis:*(-)?]
        dev-python/jellyfish[>=0.5.0][python_abis:*(-)?]
        dev-python/munkres[python_abis:*(-)?]
        dev-python/musicbrainzngs[>=0.4][python_abis:*(-)?]
        dev-python/mutagen[>=1.33][python_abis:*(-)?]
        dev-python/six[>=1.9][python_abis:*(-)?]
    test:
        dev-python/Flask[python_abis:*(-)?]
        dev-python/beautifulsoup4[python_abis:*(-)?]
        dev-python/discogs-client[>=2.2.1][python_abis:*(-)?]
        dev-python/mock[python_abis:*(-)?]
        dev-python/pylast[python_abis:*(-)?]
        dev-python/python-mpd2[python_abis:*(-)?]
        dev-python/pyxdg[python_abis:*(-)?]
        dev-python/rarfile[python_abis:*(-)?]
        dev-python/responses[python_abis:*(-)?]
    suggestion:
        dev-python/dbus-python[python_abis:*(-)?] [[ group-name = [ metasync ] ]]
        dev-python/discogs-client[>=2.2.1][python_abis:*(-)?] [[ group-name = [ discogs ] ]]
        dev-python/pylast[python_abis:*(-)?] [[ group-name = [ lastgenre ] ]]
        dev-python/python-mpd2[>=0.4.2][python_abis:*(-)?] [[ group-name = [ mpdstats ] ]]
        dev-python/pyxdg[python_abis:*(-)?] [[ group-name = [ thumbnails ] ]]
        dev-python/rarfile[python_abis:*(-)?] [[ description = [ beets importer can import directly from RAR archives ] ]]
        dev-python/requests-oauthlib[>=0.6.1][python_abis:*(-)?] [[ group-name = [ beatport ] ]]
        dev-python/requests[python_abis:*(-)?] [[ group-name = [ absubmit ] ]]
        dev-python/requests[python_abis:*(-)?] [[ group-name = [ fetchart ] ]]
        media-sound/mp3gain [[ group-name = [ replaygain ] ]]
        media/ffmpeg [[ group-name = [ convert ] ]]
        (
            gnome-desktop/gobject-introspection[python_abis:*(-)?]
            media-libs/gstreamer:1.0
        ) [[
            *group-name = [ bpd ]
        ]]
        (
            dev-python/pyacoustid[python_abis:*(-)?]
            media-libs/chromaprint
        ) [[
            *group-name = [ chroma ]
        ]]
        (
            dev-python/Flask[python_abis:*(-)?]
            dev-python/Flask-Cors[python_abis:*(-)?]
        ) [[
            *group-name = [ web ]
        ]]
"

if ever is_scm;then
    DEPENDENCIES+="
        build:
            dev-python/Sphinx[python_abis:*(-)?]
    "
fi

# many tests require internet access, and fail due to sandboxing
RESTRICT=test

beets_src_compile() {
    setup-py_src_compile
    if ever is_scm;then
        emake -C docs/ man
        edo mv docs/_build/man/ man/
    fi
}

beets_src_install() {
    setup-py_src_install

    dobashcompletion "${FILES}"/beet
    dozshcompletion extra/_beet
    doman man/*
}

