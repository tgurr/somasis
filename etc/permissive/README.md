## etc/permissive
This is a section of the repository which aims to help reach my personal goal
of a system made up of only permissively-licesned, preferably BSD-like, packages,
where possible. I believe that they are of a more-true freedom than what viral
licenses, such as the GPL, claim to achieve.

Obviously this is not entirely possible due to core components such as `paludis`,
`eclectic`, `coreutils`, etc. being licensed as GPL-2 (or GPL-3, even worse),
but the idea here is that a list of software which you may want to avoid in favor
or more permissively licensed alternatives, and to unmask software which falls
under a permissive license, such as the ISC by default.

Programs which are under viral licenses will be unmasked separately, rather than
a global unmask for any license.

## Usage
Create a symlink to `/var/db/paludis/repositories/somasis/etc/permissive/licences.conf`:

    # ln -s /var/db/paludis/repositories/somasis/etc/permissive/licences.conf /etc/paludis/licences.conf

or, if you wish to still have your own custom `licences.conf`, create a `licences.bash`
in `/etc/paludis` which concatenates the one here and a custom file of your own creation.

## Additions
If you find a package that needs to be unmasked, it should be added to
`licences.conf`; preferably using `license-unmask`, which is part of [permissive-utils].

Submit any changes to my repo on [Exherbo's Gerrit].

## Future
It is my personal goal to make Exherbo as a permissive system, viable. Akin to how
OpenBSD aims to make a truly free, permissively-licensed, and entirely free-to-modify
system for use by anyone, for any purpose, I want to make Exherbo to be this way 
in as much of a way as a Linux-based system can.

My plans are to further the amount of virtual packages in the system, by first
providing alternatives to tools which already have good replacements; `coreutils` is one
I am pursuing at the moment, with `toybox` as my favored replacement.

## Disclaimer
This is not official.
None of the things written here are supported by Exherbo developers; this is
just a personal project. Don't bother them with these issues.

[Exherbo's Gerrit]: https://galileo.mailstation.de/gerrit/
[permissive-utils]: https://github.com/Somasis/permissive-utils
