# Blacklist all licences, use whitelist instead
*/* -*

# OpenBSD accepted licences
*/* Apache-1.1 BSD BSD-2 BSD-3 BSD-4 ISC libX11 MIT public-domain X11

# BSD-like
*/* Adobe adobe-ps AFL-2.1 bzip2-withdocs cyrus-sasl dante FTL Info-ZIP lsof \
    MaxMind openssl UoI-NCSA ZLIB

# MIT-like
*/* as-is Boost-1.0 CUP ElementTree g711 icu ImageMagick imlib2 IJG JLex jq \
    PSF-2.2 PYTHON tcp_wrappers ZSH

# Allow modifications, but with restrictions (different name, can't impede
# original, etc.)
*/* Apache-2.0 Artistic Artistic-2.0

# Allow distributing, without selling by themselves (with something else
# is allowed)
*/* Arev Bitstream-Vera

# Only requires attribution
*/* CCPL-Attribution-3.0

# Public domain/no restrictions or conditions
*/* CC0 CC-PublicDomain WTFPL-2

# they're labeled FIXME for a reason! fix them!
*/* -FIXME

# Possible replacements:
#   app-misc/screen         -> app-terminal/tmux
#   dev-util/pkg-config     -> dev-util/pkgconf
#   sys-devel/gcc           -> dev-lang/clang
#   app-arch/tar            -> app-arch/libarchive
#   app-arch/cpio           -> app-arch/libarchive
#   app-arch/gzip           -> app-arch/pigz
#   sys-apps/man            -> sys-apps/mandoc
#   sys-libs/glibc          -> sys-libs/musl
#   x11-apps/xclip          -> x11-apps/xsel
#   media-gfx/ImageMagick   -> media-gfx/GraphicsMagick
#
#   */* providers: -* pkgconf lok graphicsmagick libarchive pigz mandoc
#

## Hall of Shame
# once gcc and stuff can be removed from a system without any issues
# these will be removed
sys-devel/gcc GPL-2 GPL-3
sys-libs/libatomic GPL-2
sys-libs/libgcc GPL-2
sys-libs/libstdc++ GPL-2

# Exherbo projects
# (this means maybe I can work towards getting these permissive) (lol)
app-admin/eclectic GPL-2
app-admin/eclectic-fontconfig GPL-2
app-admin/eclectic-gstreamer GPL-2
app-admin/eclectic-gcc GPL-2
app-admin/eclectic-pango GPL-2
app-admin/eclectic-python GPL-2
app-paludis/dexter GPL-2
dev-java/java-env GPL-2
dev-util/exherbo-dev-tools GPL-2
sys-apps/paludis GPL-2
sys-devel/autoconf-wrapper GPL-2
sys-devel/automake-wrapper GPL-2

# system set
app-arch/xz GPL-2 GPL-3 LGPL-2.1
app-shells/bash GPL-3
dev-scm/git GPL-2
net-misc/iputils GPL-2
net-misc/rsync GPL-3
net-misc/wget GPL-3
sys-apps/bc LGPL-2.1
sys-apps/diffutils GPL-3
sys-apps/findutils GPL-3
sys-apps/groff GPL-2
sys-apps/kbd GPL-2
sys-apps/kmod GPL-2 LGPL-2.1
sys-apps/man-pages man-pages
sys-apps/skeleton-filesystem-layout GPL-2
sys-apps/texinfo GPL-2 GPL-3
sys-apps/util-linux GPL-2 GPL-3 LGPL-2.1
sys-apps/which GPL-3
sys-devel/binutils GPL-3 LGPL-3
sys-devel/gnuconfig GPL-3
sys-devel/libtool GPL-2
sys-devel/make GPL-3
sys-devel/patch GPL-3
sys-fs/e2fsprogs GPL-2 LGPL-2
sys-kernel/linux-headers GPL-2
sys-libs/readline GPL-3
sys-process/procps GPL-2 LGPL-2
sys-process/psmisc GPL-2

# You should whitelist other packages with /etc/paludis/licences.conf.d/
